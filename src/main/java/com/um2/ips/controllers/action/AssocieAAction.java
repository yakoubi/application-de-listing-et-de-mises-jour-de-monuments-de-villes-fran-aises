package com.um2.ips.controllers.action;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.um2.ips.entities.AssocieA;
import com.um2.ips.entities.AssocieAId;
import com.um2.ips.entities.Celebrite;
import com.um2.ips.entities.Monument;
import com.um2.ips.service.AssocieAService;
import com.um2.ips.service.CelebriteService;
import com.um2.ips.service.monumentService;




@Namespace("/associeA")
public class AssocieAAction extends ActionSupport{

	
	
	@Autowired // injection 
	monumentService monumentAService;
	@Autowired
	AssocieAService associeAService;
	@Autowired
	CelebriteService celebriteService;
	
	private AssocieA associeA = new AssocieA();
	private List<AssocieA> listeAssociea = new ArrayList<AssocieA>();
	private List<Celebrite>listeCelebrites=new ArrayList<Celebrite>();
	private Map<Integer,String>listeCelebrites2=new HashMap<Integer,String>();
	private List<Celebrite>listeCelebritesParCodeM = new ArrayList<Celebrite>();
	 List<Celebrite>listeCelebritesParCodeM2 = new ArrayList<Celebrite>();

	private HashMap<Integer,String>mapCelebrite;

	
	private String Id;
	// un boolean pour différencier en tre un edite et nouveau add 
	 private boolean modeEdtit = false;
	private Monument monumentEcours;
	private int celebriteSel;
	//pour récuperer le parametre de la request 
	   private  String monumentId;
	

	
	@Action(value = "associeA", results = {
			@Result(name = SUCCESS , location = "/WEB-INF/views/addAssocieA.jsp")
		})
		public String addForm() {
		// recuperer la liste de  de ma db
		//this.listeAssociea =associeAService.listerAssocieA();
		
		getMonumentId();
		getMonumentEcours();
		
		this.listeCelebritesParCodeM =getListeCelebritesParCodeM(monumentId);
		return SUCCESS;
		}
//
	@Action(value = "addAssociea", results = {
			@Result(name = SUCCESS , location = "/WEB-INF/views/addAssocieA.jsp"),
			@Result(name = "input" , location = "/WEB-INF/views/addAssocieA.jsp")

		})
		public String addAssociea() {
	
		System.out.println("monumentId: "+monumentId);
		AssocieAId associeAId = new AssocieAId();
		associeAId.setCodeM(monumentId);
	    this.associeA.setAssocieAId(associeAId);
		associeA.getAssocieAId().setNumCelebrite(celebriteSel);
		if(associeA!=null) {
		if(modeEdtit == false) {
			 this.associeAService.addAssocieA(associeA);
			 getListeAssociea();

		}else {
//			associeAService.edit(this.associeA );
//			getListeDepartementsDep();
//			this.lieu = new Lieu();
			modeEdtit=false;
		}
		}
		this.listeAssociea= this.associeAService.listerAssocieA();
		this.listeCelebritesParCodeM =getListeCelebritesParCodeM(monumentId);

		//vider le form
		this.associeA = new AssocieA();
		
		
					 return SUCCESS;
		}
	
	@Action(value = "delete", results = {
			@Result(name = SUCCESS , location = "/WEB-INF/views/addAssocieA.jsp")
		})
		public String delete() {
		
		AssocieAId associeAId = new AssocieAId();
		associeAId.setCodeM(monumentId);
	    this.associeA.setAssocieAId(associeAId);
		associeA.getAssocieAId().setNumCelebrite(celebriteSel);
			associeAService.delete(associeA.getAssocieAId());
			this.listeAssociea= this.associeAService.listerAssocieA();
			
			this.listeCelebritesParCodeM =getListeCelebritesParCodeM(monumentId);

			return SUCCESS;
		}
//		
//		@Action(value = "edit", results = {
//				@Result(name = SUCCESS , location = "/WEB-INF/views/addLieu.jsp")
//			})
//		public String edit() {
//			this.modeEdtit=true;
//			this.lieu = associeAService.find(lieuId);
//			this.listeAssociea= this.associeAService.listerLieux();
//
//			this.listeDepartements= this.departementService.listerDepatements();
//
//			return SUCCESS;
//		}
		
		
		
//		
//		public List<String> getListeDepartementsDep() {
//			this.listeDepartementsDep=new ArrayList<String>();
//			this.listeDepartements= this.departementService.listerDepatements();
//			for(Departement element: listeDepartements) {
//				this.listeDepartementsDep.add(element.getDep());
//			}
//			return listeDepartementsDep;
//			
//		}
	
	

		
		
		public AssocieAService getAssocieAService() {
			return associeAService;
		}
		public void setAssocieAService(AssocieAService associeAService) {
			this.associeAService = associeAService;
		}
		public AssocieA getAssocieA() {
			return associeA;
		}
		public void setAssocieA(AssocieA associeA) {
			this.associeA = associeA;
		}
		public List<AssocieA> getListeAssociea() {
			return listeAssociea;
		}
		public void setListeAssociea(List<AssocieA> listeAssociea) {
			this.listeAssociea = listeAssociea;
		}
		
		public String getId() {
			return Id;
		}
		public void setId(String id) {
			Id = id;
		}
		public boolean isModeEdtit() {
			return modeEdtit;
		}
		public void setModeEdtit(boolean modeEdtit) {
			this.modeEdtit = modeEdtit;
		}
		
		public int getCelebriteSel() {
			return celebriteSel;
		}
		public void setCelebriteSel(int celebriteSel) {
			this.celebriteSel = celebriteSel;
		}
		public List<Celebrite> getListeCelebrites() {
			listeCelebrites =celebriteService.listerCelebrites();
			
			return listeCelebrites;
		}
		public Map<Integer,String>getListeCelebrites2() {
			this.listeCelebrites2=new HashMap<Integer,String>();
			listeCelebrites =celebriteService.listerCelebrites();
			for(Celebrite element: listeCelebrites) {
				this.listeCelebrites2.put(element.getNumCelebrite(),element.getNom());
			}
			return listeCelebrites2;
			
		}
		
		
		
		public void setListeCelebrites(List<Celebrite> listeCelebrites) {
			this.listeCelebrites = listeCelebrites;
		}
		
		
		
		
		public HashMap<Integer, String> getMapCelebrite() {
			mapCelebrite=new HashMap<Integer, String>();
			for (Celebrite cel : getListeCelebrites()) {
				mapCelebrite.put(cel.getNumCelebrite(), cel.getNom());
			}
			return mapCelebrite;
		}
		public void setMapCelebrite(HashMap<Integer, String> mapCelebrite) {
			this.mapCelebrite = mapCelebrite;
		}
		
		
		
		public Monument getMonumentEcours() {
			if(monumentId!=null) {
				monumentEcours = monumentAService.find(monumentId);
			}else {
				addActionError("aucun monument selectioné ");

			}
			return monumentEcours;
		}
		public void setMonumentEcours(Monument monumentEcours) {
			this.monumentEcours = monumentEcours;
		}
		public String getMonumentId() {
		///Map x=	ActionContext.getContext().getParameters();
			//recuperer le momenument en cours grace in momunent passer en param
			HttpServletRequest request = ServletActionContext.getRequest();
			monumentId =request.getParameter("monumentId");
			AssocieAId associeAId = new AssocieAId();
			associeAId.setCodeM(monumentId);
		this.associeA.setAssocieAId(associeAId);
		
			return monumentId;
		}
		public void setMonumentId(String monumentId) {
			this.monumentId = monumentId;
		}
		
		
		
		
		public List<Celebrite> getListeCelebritesParCodeM(String codeM) {
			listeCelebritesParCodeM =associeAService.listeCelebriteAssocieMonument(codeM);
			return listeCelebritesParCodeM;
		}
		public void setListeCelebritesParCodeM(List<Celebrite> listeCelebritesParCodeM) {
			this.listeCelebritesParCodeM = listeCelebritesParCodeM;
		}
		public List<Celebrite> getListeCelebritesParCodeM2() {
			return listeCelebritesParCodeM2;
		}
		public void setListeCelebritesParCodeM2(List<Celebrite> listeCelebritesParCodeM2) {
			this.listeCelebritesParCodeM2 = listeCelebritesParCodeM2;
		}
		public List<Celebrite> getListeCelebritesParCodeM() {
			return listeCelebritesParCodeM;
		}
		public void setListeCelebrites2(Map<Integer,String>listeCelebrites2) {
			this.listeCelebrites2 = listeCelebrites2;
		}

	
	
}
