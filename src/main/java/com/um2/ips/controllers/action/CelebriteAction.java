package com.um2.ips.controllers.action;


import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.um2.ips.entities.Celebrite;
import com.um2.ips.service.CelebriteService;



@Namespace("/celebrite")

public class CelebriteAction extends ActionSupport{

	
	
	@Autowired // injection 
	CelebriteService celebriteService;
	
	private Celebrite celebrite = new Celebrite();
	private List<Celebrite> listeCelebrites = new ArrayList<Celebrite>();
	
	private int celebriteId;
	// un boolean pour différencier en tre un edite et nouveau add 
	 private boolean modeEdtit = false;
	
	
	



	@Action(value = "celebrites", results = {
			@Result(name = SUCCESS , location = "/WEB-INF/views/addCelebrite.jsp")
		})
		public String addForm() {
		// recuperer la liste de deparetement de ma db
		this.listeCelebrites =celebriteService.listerCelebrites();
		celebrite = new Celebrite();
					 return SUCCESS;
		}
//
	@Action(value = "addCelebrite", results = {
			@Result(name = SUCCESS , location = "/WEB-INF/views/addCelebrite.jsp"),
			@Result(name = "input" , location = "/WEB-INF/views/addCelebrite.jsp")

		})
		public String addCelebrite() {
		if(celebrite!=null) {
		if(modeEdtit == false) {
			 this.celebriteService.addCelebrite(celebrite);
			

		}else {
			celebriteService.edit(this.celebrite );
			this.celebrite = new Celebrite();
			modeEdtit=false;
		}
		}
		this.listeCelebrites= this.celebriteService.listerCelebrites();
		 this.celebrite = new Celebrite();
					 return SUCCESS;
		}
	
	@Action(value = "delete", results = {
			@Result(name = SUCCESS , location = "/WEB-INF/views/addCelebrite.jsp")
		})
		public String delete() {
			celebriteService.delete(celebriteId);
			this.listeCelebrites= this.celebriteService.listerCelebrites();
			return SUCCESS;
		}
		
		@Action(value = "edit", results = {
				@Result(name = SUCCESS , location = "/WEB-INF/views/addCelebrite.jsp")
			})
		public String edit() {
			this.modeEdtit=true;
			this.celebrite = celebriteService.find(celebriteId);
			this.listeCelebrites= this.celebriteService.listerCelebrites();

			//CelebriteService.edit(depId, this.dep );
			//this.listeCelebrites= this.CelebriteService.listerDepatements();

			return SUCCESS;
		}
		public Celebrite getCelebrite() {
			return celebrite;
		}
		public void setCelebrite(Celebrite celebrite) {
			this.celebrite = celebrite;
		}
		public List<Celebrite> getListeCelebrites() {
			return listeCelebrites;
		}
		public void setListeCelebrites(List<Celebrite> listeCelebrites) {
			this.listeCelebrites = listeCelebrites;
		}
		public int getCelebriteId() {
			return celebriteId;
		}
		public void setCelebriteId(int celebriteId) {
			this.celebriteId = celebriteId;
		}
		public boolean isModeEdtit() {
			return modeEdtit;
		}
		public void setModeEdtit(boolean modeEdtit) {
			this.modeEdtit = modeEdtit;
		}
		

	



	
	
}
