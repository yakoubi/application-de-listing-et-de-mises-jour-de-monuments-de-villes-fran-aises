package com.um2.ips.controllers.action;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.um2.ips.entities.Departement;
import com.um2.ips.service.DepartementService;



@Namespace("/departement")

public class DepartementAction extends ActionSupport{

	
	
	@Autowired // injection 
	DepartementService departementService;
	
	private Departement dep = new Departement();
	private List<Departement> listeDepartements = new ArrayList<Departement>();
	
	private String depId;
	// un boolean pour différencier en tre un edite et nouveau add 
	 private boolean modeEdtit = false;
	
	
	



	@Action(value = "departements", results = {
			@Result(name = SUCCESS , location = "/WEB-INF/views/addDepartement.jsp")
		})
		public String addForm() {
		// recuperer la liste de deparetement de ma db
		this.listeDepartements =departementService.listerDepatements();
		
		Map session=ActionContext.getContext().getSession();
		Object role1= session.get("logined");
		Object role2=  session.get("user");
					 return SUCCESS;
		}
//
	@Action(value = "addDepartement", results = {
			@Result(name = SUCCESS , location = "/WEB-INF/views/addDepartement.jsp"),
			@Result(name = "input" , location = "/WEB-INF/views/addDepartement.jsp")

		})
		public String addDepartement() {
		if(dep!=null) {
		if(modeEdtit == false) {
			 this.departementService.addDepartement(dep);

		}else {
			departementService.edit(this.dep );
			this.dep = new Departement();
			modeEdtit=false;
		}
		}
		this.listeDepartements= this.departementService.listerDepatements();

					 return SUCCESS;
		}
	
	@Action(value = "delete", results = {
			@Result(name = SUCCESS , location = "/WEB-INF/views/addDepartement.jsp")
		})
		public String delete() {
			departementService.delete(depId);
			this.listeDepartements= this.departementService.listerDepatements();
			return SUCCESS;
		}
		
		@Action(value = "edit", results = {
				@Result(name = SUCCESS , location = "/WEB-INF/views/addDepartement.jsp")
			})
		public String edit() {
			this.modeEdtit=true;
			this.dep = departementService.find(depId);
			this.listeDepartements= this.departementService.listerDepatements();

			//departementService.edit(depId, this.dep );
			//this.listeDepartements= this.departementService.listerDepatements();

			return SUCCESS;
		}
		

		public String getDepId() {
			return depId;
		}

		public void setDepId(String depId) {
			this.depId = depId;
		}

		public List<Departement> getListeDepartements() {
			return listeDepartements;
		}

		public void setListeDepartements(List<Departement> listeDepartements) {
			this.listeDepartements = listeDepartements;
		}
		
		public boolean getModeEdtit() {
			return modeEdtit;
		}

		public void setModeEdtit(boolean modeEdtit) {
			this.modeEdtit = modeEdtit;
		}

	public Departement getDep() {
		return dep;
	}


	public void setDep(Departement dep) {
		this.dep = dep;
	}



	
	
}
