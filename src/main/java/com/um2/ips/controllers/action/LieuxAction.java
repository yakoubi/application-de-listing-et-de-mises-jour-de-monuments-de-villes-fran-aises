package com.um2.ips.controllers.action;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.um2.ips.entities.Departement;
import com.um2.ips.entities.Lieu;
import com.um2.ips.service.DepartementService;
import com.um2.ips.service.LieuService;



@Namespace("/lieu")

public class LieuxAction extends ActionSupport{

	
	
	@Autowired // injection 
	DepartementService departementService;
	@Autowired
	LieuService lieuService;
	
	private Lieu lieu = new Lieu();
	private List<Lieu> listeLieux = new ArrayList<Lieu>();
	private List<Departement>listeDepartements=new ArrayList<Departement>();
	private Map<String,String>listeDepartementsDep2 = new HashMap<String,String>();
	
	private String lieuId;
	// un boolean pour différencier en tre un edite et nouveau add 
	 private boolean modeEdtit = false;
	
	private String depSelect;
	

	

	
	@Action(value = "lieux", results = {
			@Result(name = SUCCESS , location = "/WEB-INF/views/addLieu.jsp")
		})
		public String addForm() {
		// recuperer la liste de deparetement de ma db
		this.listeLieux =lieuService.listerLieux();
		
					 return SUCCESS;
		}
//
	@Action(value = "addLieu", results = {
			@Result(name = SUCCESS , location = "/WEB-INF/views/addLieu.jsp"),
			@Result(name = "input" , location = "/WEB-INF/views/addLieu.jsp")

		})
		public String addLieu() {
		if(lieu!=null) {
		if(modeEdtit == false) {
			 this.lieuService.addlieu(lieu);
			 getListeDepartementsDep2();

		}else {
			lieuService.edit(this.lieu );
			getListeDepartementsDep2();
			this.lieu = new Lieu();
			modeEdtit=false;
		}
		}
		this.listeLieux= this.lieuService.listerLieux();
		//vider le form
		this.lieu = new Lieu();
		
		
					 return SUCCESS;
		}
	
	@Action(value = "delete", results = {
			@Result(name = SUCCESS , location = "/WEB-INF/views/addLieu.jsp")
		})
		public String delete() {
			lieuService.delete(lieuId);
			this.listeLieux= this.lieuService.listerLieux();
			return SUCCESS;
		}
		
		@Action(value = "edit", results = {
				@Result(name = SUCCESS , location = "/WEB-INF/views/addLieu.jsp")
			})
		public String edit() {
			this.modeEdtit=true;
			this.lieu = lieuService.find(lieuId);
			this.listeLieux= this.lieuService.listerLieux();

			this.listeDepartements= this.departementService.listerDepatements();

			return SUCCESS;
		}
		
		
		
		
		public Lieu getLieu() {
			return lieu;
		}
		public void setLieu(Lieu lieu) {
			this.lieu = lieu;
		}
		public List<Lieu> getListeLieux() {
			return listeLieux;
		}
		public void setListeLieux(List<Lieu> listeLieux) {
			this.listeLieux = listeLieux;
		}
		public String getLieuId() {
			return lieuId;
		}
		public void setLieuId(String lieuId) {
			this.lieuId = lieuId;
		}
		public boolean isModeEdtit() {
			return modeEdtit;
		}
		public void setModeEdtit(boolean modeEdtit) {
			this.modeEdtit = modeEdtit;
		}
		
		
		public List<Departement> getListeDepartements() {
			return listeDepartements;
		}
		public void setListeDepartements(List<Departement> listeDepartements) {
			this.listeDepartements = listeDepartements;
		}

	
		public String getDepSelect() {
			return depSelect;
		}
		public void setDepSelect(String depSelect) {
			this.depSelect = depSelect;
		}

		

		/////////////////////////////////
		public  Map<String, String>  getListeDepartementsDep2() {
			this.listeDepartementsDep2=new HashMap<String,String>();;
			this.listeDepartements= this.departementService.listerDepatements();
			for(Departement element: listeDepartements) {
				this.listeDepartementsDep2.put(element.getDep(),element.getNomDep());
			}
			return listeDepartementsDep2;
			
		}
		public void setListeDepartementsDep2(Map<String, String> listeDepartementsDep2) {
			this.listeDepartementsDep2 = listeDepartementsDep2;
		}
	
	
}
