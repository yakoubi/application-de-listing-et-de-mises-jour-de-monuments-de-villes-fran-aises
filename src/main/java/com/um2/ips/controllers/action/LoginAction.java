package com.um2.ips.controllers.action;

import java.util.Map;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.um2.ips.entities.User;
import com.um2.ips.service.UserService;

import net.bytebuddy.implementation.bind.annotation.DefaultMethod;

public class LoginAction  extends ActionSupport{
	@Autowired
	UserService userService;
 private String email;
 private String password;
 public String message;
 
	@Action(value = "home", results = {
			@Result(name = SUCCESS , location = "/index.jsp")
		
		}
			)
		public String home() {
	 return SUCCESS;
	}
 
 
	@Action(value = "/loginForm", results = {
			@Result(name = SUCCESS , location = "/WEB-INF/views/login.jsp")
		})
		public String index() {
	 return SUCCESS;
	}
 
	
	@Action(value = "login", results = {
			@Result(name = SUCCESS ,type = "redirectAction" ,params = {"actionName","home","namespace","/"}),

			@Result(name = ERROR , location = "/WEB-INF/views/login.jsp"),
			@Result(name = INPUT , location = "/WEB-INF/views/login.jsp")

			
		})
		public String autentification() {
		// tester la pr�sence du couple login/password ds la base
		if(email!=null && password!=null) {
		User user=userService.getUser(email, password);
		if(user!=null) {
			//creation d'une session
			Map session=ActionContext.getContext().getSession();
			
			session.put("logined","true");
			session.put("user",user);
			return SUCCESS;
		}else {
			addActionError("vous n'�tes pas autentifi� ");

			return ERROR;
		}

	}else {
	return INPUT;	
	}
	}
	
	
	
	/**
	 *			//@Result(name = "SUCCESS" ,type = "redirectAction" ,params = {"actionName","home","namespace","/"})

	 * @return
	 */
	@Action(value = "logout", results = {
			 @Result(name = SUCCESS , location = "/WEB-INF/views/login.jsp")

		})
		public String logout() {
		
		User user=userService.getUser(email, password);
			Map session=ActionContext.getContext().getSession();
			
			//je vide ma session
			session.remove("logined");
			session.remove("user");
			message ="vous etre deconnect�";
			System.out.println(message);

			return SUCCESS;
			
		}
	

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
	
	
}
