package com.um2.ips.controllers.action;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.um2.ips.entities.Departement;
import com.um2.ips.entities.Lieu;
import com.um2.ips.entities.Monument;
import com.um2.ips.service.LieuService;
import com.um2.ips.service.monumentService;


@Namespace("/monument")
public class MonumentAction extends ActionSupport{

	
	@Autowired
	monumentService monumentService;
	@Autowired
	LieuService lieuService;
	
	private Monument monument = new Monument();
	private List<Monument> monuments = new ArrayList<Monument>();
	private Map<String,String>listeLieuxCodeInsee=new HashMap<String,String>();
	private List<Lieu> lieux = new ArrayList<Lieu>();
	private String monumentId;
//un boolean pour différencier en tre un edite et nouveau add 
	 private boolean modeEdtit = false;
	private String depSelect;
	//pour calculer la distance
	private Monument monument1= new Monument();
	private Monument monument2= new Monument();
    private double distance;
	
	@Action(value = "index", results = {
			@Result(name = SUCCESS , location = "/WEB-INF/views/index.jsp")
		})
		public String index() {
			 this.monuments = monumentService.findAll();
			return SUCCESS;
		}
	
	/**
	 * 
	 * @return
	 */

	@Action(value = "monuments", results = {
			@Result(name = SUCCESS , location = "/WEB-INF/views/addMonument.jsp")
		})
		public String addForm() {
		// recuperer la liste de deparetement de ma db
		this.monuments =monumentService.listerMonuments();
		
					 return SUCCESS;
		}
//
	
	/**
	 * 
	 * @return
	 */
	@Action(value = "addMonument", results = {
			@Result(name = SUCCESS , location = "/WEB-INF/views/addMonument.jsp"),
			@Result(name = "input" , location = "/WEB-INF/views/addMonument.jsp")

		})
		public String addMonument() {
		if(monument!=null) {
		if(modeEdtit == false) {
			 this.monumentService.addMonument(monument);
			 getListeLieuxCodeInsee();

		}else {
			monumentService.edit(this.monument );
			getListeLieuxCodeInsee();
			this.monument = new Monument();
			modeEdtit=false;
		}
		}
		this.monuments= this.monumentService.listerMonuments();
		//vider le form
		this.monument = new Monument();
		
		
					 return SUCCESS;
		}
	
	
	/**
	 * 
	 * @return
	 */
	@Action(value = "delete", results = {
			@Result(name = SUCCESS , location = "/WEB-INF/views/addMonument.jsp")
		})
		public String delete() {
			monumentService.delete(monumentId);
			this.monuments= this.monumentService.listerMonuments();
			return SUCCESS;
		}
		/**
		 * 
		 * @return
		 */
		@Action(value = "edit", results = {
				@Result(name = SUCCESS , location = "/WEB-INF/views/addMonument.jsp")
			})
		public String edit() {
			this.modeEdtit=true;
			this.monument = monumentService.find(monumentId);
			this.monuments= this.monumentService.listerMonuments();

			return SUCCESS;
		}
		
		
		@Action(value = "distance", results = {
				@Result(name = SUCCESS , location = "/WEB-INF/views/addMonument.jsp")
			})
		public String distance() {
			
			this.monument1 = monumentService.find(monument1.getCodeM());
			this.monument2 = monumentService.find(monument2.getCodeM());
			if (monument1!= null && monument2!= null) {
				this.distance =monumentService.calculerDistance(monument1, monument2);

			}else {
				addActionError("vous devez choisir deux code de monument existant dans le du tableau");
			}
			this.monuments= this.monumentService.listerMonuments();

			return SUCCESS;
		}
		
		
		
		

		public Monument getMonument() {
			return monument;
		}

		public void setMonument(Monument monument) {
			this.monument = monument;
		}

		public List<Monument> getMonuments() {
			return monuments;
		}

		public void setMonuments(List<Monument> monuments) {
			this.monuments = monuments;
		}

		public Map<String,String> getListeLieuxCodeInsee() {
			this.listeLieuxCodeInsee=new HashMap<String,String>();
			this.lieux= this.lieuService.listerLieux();
			for(Lieu element: lieux) {
				this.listeLieuxCodeInsee.put(element.getCodeInsee(),element.getNomCom());
			}
			return listeLieuxCodeInsee;		}

		public void setListeLieuxCodeInsee(Map<String,String> listeLieuxCodeInsee) {
			this.listeLieuxCodeInsee = listeLieuxCodeInsee;
					}

		

		public String getMonumentId() {
			return monumentId;
		}

		public void setMonumentId(String monumentId) {
			this.monumentId = monumentId;
		}

		public boolean isModeEdtit() {
			return modeEdtit;
		}

		public void setModeEdtit(boolean modeEdtit) {
			this.modeEdtit = modeEdtit;
		}

		public String getDepSelect() {
			return depSelect;
		}

		public void setDepSelect(String depSelect) {
			this.depSelect = depSelect;
		}

		public List<Lieu> getLieux() {
			return lieux;
		}

		public void setLieux(List<Lieu> lieux) {
			this.lieux = lieux;
		}

		public Monument getMonument1() {
			return monument1;
		}

		public void setMonument1(Monument monument1) {
			this.monument1 = monument1;
		}

		public Monument getMonument2() {
			return monument2;
		}

		public void setMonument2(Monument monument2) {
			this.monument2 = monument2;
		}

		public double getDistance() {
			return distance;
		}

		public void setDistance(double distance) {
			this.distance = distance;
		}
		
		







	



	
	
}
