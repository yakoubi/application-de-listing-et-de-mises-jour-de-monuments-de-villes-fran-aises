package com.um2.ips.controllers.action;


import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.components.ActionMessage;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.um2.ips.entities.User;
import com.um2.ips.service.UserService;



@Namespace("/user")

public class UserAction extends ActionSupport{

	
	
	@Autowired // injection 
	UserService userService;
	
	private User user = new User();
	private List<User> listeUsers = new ArrayList<User>();
	
	private int idUser;
	// un boolean pour diff�rencier en tre un edite et nouveau add 
	 private boolean modeEdtit = false;
	
	
	



	@Action(value = "users", results = {
			@Result(name = SUCCESS , location = "/WEB-INF/views/addUser.jsp")
		})
		public String addForm() {
		// recuperer la liste de deparetement de ma db
		this.listeUsers =userService.listerUsers();
					 return SUCCESS;
		}
//
	@Action(value = "addUser", results = {
			@Result(name = SUCCESS , location = "/WEB-INF/views/addUser.jsp"),
			@Result(name = "input" , location = "/WEB-INF/views/addUser.jsp"),
			@Result(name = "bravo" ,type = "redirectAction" ,params = {"actionName","home","namespace","/"})


		})
		public String addUser() {
		if(user!=null) {
		if(modeEdtit == false) {
			//par d�faut l'utiliszteur cr�er a le role touriste
			if(user.getRole()==null) {
				this.user.setRole("touriste");
			}
			 this.userService.addUser(user);

		}else {
			userService.edit(this.user );
			this.user = new User();
			modeEdtit=false;
		}
		}
		this.listeUsers= this.userService.listerUsers();
		
		addActionMessage("compte cr�er avec succss bravo !");
		if(user.getRole().equals("touriste")) {
			return "bravo";
		}
		user = new User();//on vide le formulaire
					 return SUCCESS;
		}
	
	@Action(value = "delete", results = {
			@Result(name = SUCCESS , location = "/WEB-INF/views/addUser.jsp")
		})
		public String delete() {
			userService.delete(idUser);
			this.listeUsers= this.userService.listerUsers();
			return SUCCESS;
		}
		
		@Action(value = "edit", results = {
				@Result(name = SUCCESS , location = "/WEB-INF/views/addUser.jsp")
			})
		public String edit() {
			this.modeEdtit=true;
			this.user = userService.find(idUser);
			this.listeUsers= this.userService.listerUsers();


			return SUCCESS;
		}
		

		
	

		public User getUser() {
			return user;
		}
		public void setUser(User user) {
			this.user = user;
		}
		public int getIdUser() {
			return idUser;
		}
		public void setIdUser(int idUser) {
			this.idUser = idUser;
		}
		public List<User> getListeUsers() {
			return listeUsers;
		}

		public void setListeUsers(List<User> listeUsers) {
			this.listeUsers = listeUsers;
		}
		
		public boolean getModeEdtit() {
			return modeEdtit;
		}

		public void setModeEdtit(boolean modeEdtit) {
			this.modeEdtit = modeEdtit;
		}

	



	
	
}
