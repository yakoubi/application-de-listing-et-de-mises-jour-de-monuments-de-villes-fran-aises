package com.um2.ips.entities;

import java.io.Serializable;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;

@Entity
public class AssocieA implements Serializable{
	
	@Id
	@Embedded
	private AssocieAId associeAId;
	
	

//	 @ManyToOne //possibilte de plusieurs associations de monuments vers une celebrite
//	 @MapsId("numCelebrite")
//	 @JoinColumn(name="numCelebrite")
//	private Celebrite celebrite;
//	 
//	 @ManyToOne //possibilte de plusieurs associations de celebrites vers un monument
//	 @MapsId("codeM")
//	 @JoinColumn(name="codeM")
//	private Monument monument;
//	
	
	
	//Constructeur sans parametres
	public AssocieA() {
		super();
	}

	public AssocieA(AssocieAId associeAId) {
		super();
		this.associeAId = associeAId;
	}

	public AssocieAId getAssocieAId() {
		return associeAId;
	}

	public void setAssocieAId(AssocieAId associeAId) {
		this.associeAId = associeAId;
	}

//	public Celebrite getCelebrite() {
//		return celebrite;
//	}
//
//	public void setCelebrite(Celebrite celebrite) {
//		this.celebrite = celebrite;
//	}
//
//	public Monument getMonument() {
//		return monument;
//	}
//
//	public void setMonument(Monument monument) {
//		this.monument = monument;
//	}

	
	
	

}
