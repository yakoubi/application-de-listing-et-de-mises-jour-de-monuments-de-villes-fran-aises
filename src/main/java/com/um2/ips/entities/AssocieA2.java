package com.um2.ips.entities;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class AssocieA2  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private AssocieAID2 associeAID;
	
	
	 @ManyToOne //possibilte de plusieurs associations de monuments vers une celebrite
	 @JoinColumn(name="FK_NumCeleb")
	private Celebrite celebrite;
	 
	 @ManyToOne //possibilte de plusieurs associations de celebrites vers un monument
	 @JoinColumn(name="FK_CodeM")
	private Monument monument;

	public AssocieA2() {
	}

	public AssocieA2(Celebrite celebrite, Monument monument) {
		super();
		this.celebrite = celebrite;
		this.monument = monument;
	}

	public AssocieAID2 getAssocieAID() {
		return associeAID;
	}

	public void setAssocieAID(AssocieAID2 associeAID) {
		this.associeAID = associeAID;
	}

	public Celebrite getCelebrite() {
		return celebrite;
	}

	public void setCelebrite(Celebrite celebrite) {
		this.celebrite = celebrite;
	}

	public Monument getMonument() {
		return monument;
	}

	public void setMonument(Monument monument) {
		this.monument = monument;
	}
	 
	
}
