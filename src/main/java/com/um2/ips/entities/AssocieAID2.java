package com.um2.ips.entities;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class AssocieAID2  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		private Celebrite celebrite;
		
	 	private Monument monument;
	 	

	public AssocieAID2() {
	}

	public AssocieAID2(Celebrite celebrite, Monument monument) {
		super();
		this.celebrite = celebrite;
		this.monument = monument;
	}
	 
	
}
