package com.um2.ips.entities;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class AssocieAId  implements Serializable{
	
	private String codeM;
	private int numCelebrite;
	
	 	

	public AssocieAId() {
	}



	public AssocieAId(String codeM, int numCelebrite) {
		super();
		this.codeM = codeM;
		this.numCelebrite = numCelebrite;
	}



	public String getCodeM() {
		return codeM;
	}



	public void setCodeM(String codeM) {
		this.codeM = codeM;
	}



	public int getNumCelebrite() {
		return numCelebrite;
	}



	public void setNumCelebrite(int numCelebrite) {
		this.numCelebrite = numCelebrite;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codeM == null) ? 0 : codeM.hashCode());
		result = prime * result + numCelebrite;
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AssocieAId other = (AssocieAId) obj;
		if (codeM == null) {
			if (other.codeM != null)
				return false;
		} else if (!codeM.equals(other.codeM))
			return false;
		if (numCelebrite != other.numCelebrite)
			return false;
		return true;
	}



	@Override
	public String toString() {
		return "AssocieAId [codeM=" + codeM + ", numCelebrite=" + numCelebrite + "]";
	}

	
}
