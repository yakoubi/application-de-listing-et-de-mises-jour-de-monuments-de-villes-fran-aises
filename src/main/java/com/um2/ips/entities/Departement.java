package com.um2.ips.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
@Entity
public class Departement implements Serializable {
	 /**
	  * 
	*/
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="")
	private String dep;
	private String chefLieu;
	private String nomDep;
	private String region;
	
	 @OneToMany(mappedBy="dep",fetch=FetchType.LAZY)
	private Collection<Lieu> lieux;
	
	public Departement() {
		super();
	}
	
	public Departement(String dep, String chefLieu, String nomDep, String region) {
		super();
		this.dep = dep;
		this.chefLieu = chefLieu;
		this.nomDep = nomDep;
		this.region = region;
	}
	
//	public Collection<Lieu> getLieux() {
//		return lieux;
//	}
//
//	public void setLieux(Collection<Lieu> lieux) {
//		this.lieux = lieux;
//	}

	public String getDep() {
		return dep;
	}
	public void setDep(String dep) {
		this.dep = dep;
	}
	public String getChefLieu() {
		return chefLieu;
	}
	public void setChefLieu(String chefLieu) {
		this.chefLieu = chefLieu;
	}
	public String getNomDep() {
		return nomDep;
	}
	public void setNomDep(String nomDep) {
		this.nomDep = nomDep;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	@Override
	public String toString() {
		return "Departement [dep=" + dep + ", chefLieu=" + chefLieu + ", nomDep=" + nomDep + ", region=" + region
				+ ", lieux=" + lieux + "]";
	}

	
	
}
