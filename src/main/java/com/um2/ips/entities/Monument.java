package com.um2.ips.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Monument implements Serializable {
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	private String codeM;
	private String nomM;
	private String proprietaire;
	private String typeMonument;
	private float latitude;
	private float longitude;
	 @ManyToOne
	 @JoinColumn(name="codeLieu")
	private Lieu localite;
	
	 
//	 @OneToMany(mappedBy = "monument")
//	private List<AssocieA> associeA;
	 
	public Monument() {
		super();
	}

	public Monument(String codeM, String nomM, String proprietaire, String typeMonument, float latitude, float longitude,
			Lieu localite) {
		super();
		this.codeM = codeM;
		this.nomM = nomM;
		this.proprietaire = proprietaire;
		this.typeMonument = typeMonument;
		this.latitude = latitude;
		this.longitude = longitude;
		this.localite = localite;
	}
	

	public String getCodeM() {
		return codeM;
	}

	public void setCodeM(String codeM) {
		this.codeM = codeM;
	}

	public String getNomM() {
		return nomM;
	}
	public void setNomM(String nomM) {
		this.nomM = nomM;
	}
	public String getProprietaire() {
		return proprietaire;
	}
	public void setProprietaire(String proprietaire) {
		this.proprietaire = proprietaire;
	}
	public float getLatitude() {
		return latitude;
	}
	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}
	

	public String getTypeMonument() {
		return typeMonument;
	}

	public void setTypeMonument(String typeMonument) {
		this.typeMonument = typeMonument;
	}

	public float getLongitude() {
		return longitude;
	}

	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}

	public Lieu getLocalite() {
		return localite;
	}

	public void setLocalite(Lieu localite) {
		this.localite = localite;
	}

//	public List<AssocieA> getAssocieA() {
//		return associeA;
//	}
//
//	public void setAssocieA(List<AssocieA> associeA) {
//		this.associeA = associeA;
//	}
	
	
	
}
