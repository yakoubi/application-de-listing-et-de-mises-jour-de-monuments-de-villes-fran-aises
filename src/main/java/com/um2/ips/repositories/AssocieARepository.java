package com.um2.ips.repositories;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.um2.ips.entities.AssocieA;
import com.um2.ips.entities.AssocieAId;
import com.um2.ips.entities.Departement;
import com.um2.ips.entities.Lieu;


@Repository
public class AssocieARepository {

	@PersistenceContext
	private EntityManager entityManager;
	
	
	/*
	 * ajouter un associeA ds la base 
	 */
	public void insertAssocieA(AssocieA associeA){
		entityManager.persist(associeA);
	}
	
	
	public List<AssocieA> listerAssocieA(){
		List<AssocieA> listeAssocieA = new ArrayList<AssocieA>();
		try {
			listeAssocieA=	entityManager.createQuery("select l from AssocieA l").getResultList();

		} catch (Exception e) {
			System.out.println("probleme de recuperation de la liste des AssocieA de la base"+e);
		}
		 return listeAssocieA;
		
		
	}
	
	/*
	 * suprimer un lieu
	 */
	public void delete(AssocieAId associeA){
		AssocieA associeASupp= findAssocieA(associeA);
		entityManager.remove(associeASupp);
	}
	
	public AssocieA  findAssocieA(AssocieAId associeA){
		AssocieA associeAtrouve= entityManager.find(AssocieA.class, associeA);
return associeAtrouve;       
	}
	
	
	 public List<AssocieA>   findListNumSelecriteByCdeM(String codeM){
		 Query query= entityManager.createQuery("select l from AssocieA l  where l.associeAId.codeM=:codeM");
			query.setParameter("codeM", codeM);
List<AssocieA> liste = query.getResultList();

return liste;
	  }
	
	
	
}
