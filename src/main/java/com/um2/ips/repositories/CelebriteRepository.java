package com.um2.ips.repositories;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.um2.ips.entities.Celebrite;


@Repository
public class CelebriteRepository {

	@PersistenceContext
	private EntityManager entityManager;
	
	
	/*
	 * ajouter un celebrite ds la base 
	 */
	public void insertcelebrite(Celebrite celebrite){
		entityManager.persist(celebrite);
	}
	
	
	public List<Celebrite> listerCelebrites(){
		List<Celebrite> listeCelebrite = new ArrayList<Celebrite>();
		try {
			listeCelebrite=	entityManager.createQuery("select d from Celebrite d").getResultList();

		} catch (Exception e) {
			System.out.println("problème de récupération de la liste des celebrite de la base"+e);
		}
		 return listeCelebrite;
		
		
	}
	

	public void delete(int celebrite){
		Celebrite celebriteSupp= findCelebrite(celebrite);
		entityManager.remove(celebriteSupp);
	}
	
	public Celebrite  findCelebrite(int celebrite){
		Celebrite celeb= entityManager.find(Celebrite.class, celebrite);
return celeb;
	}
	
	
	public void edit( Celebrite newCelebrite) {
		Query query = entityManager.createQuery("update Celebrite d set d.nom =:nom ,"
				+ "d.prenom=:prenom, d.nationalite=:nationalite , d.epoque=:epoque "
				+ "where d.numCelebrite =:numCelebrite");
	
	query.setParameter("numCelebrite", newCelebrite.getNumCelebrite());
	query.setParameter("nom", newCelebrite.getNom());

	query.setParameter("prenom", newCelebrite.getPrenom());
	query.setParameter("nationalite", newCelebrite.getNationalite());
	query.setParameter("epoque", newCelebrite.getEpoque());


	
	int nbrUpdate = query.executeUpdate();
	System.out.println(nbrUpdate);

}
}
