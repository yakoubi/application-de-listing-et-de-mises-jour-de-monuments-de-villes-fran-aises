package com.um2.ips.repositories;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.um2.ips.entities.Departement;


@Repository
public class DepartementRepository {

	@PersistenceContext
	private EntityManager entityManager;
	
	
	/*
	 * ajouter un departement ds la base 
	 */
	public void insertDep(Departement dep){
		entityManager.persist(dep);
	}
	
	
	public List<Departement> listerDepartements(){
		List<Departement> listeDepartement = new ArrayList<Departement>();
		try {
			listeDepartement=	entityManager.createQuery("select d from Departement d").getResultList();

		} catch (Exception e) {
			System.out.println("problème de récupération de la liste des departements de la base"+e);
		}
		 return listeDepartement;
		
		
	}
	
	/*
	 * ajouter un departement ds la base 
	 */
	public void delete(String dep){
		Departement departSupp= findDep(dep);
		entityManager.remove(departSupp);
	}
	
	public Departement  findDep(String dep){
		Departement departement= entityManager.find(Departement.class, dep);
return departement;
	}
	
	
	public void edit( Departement newDepartement) {
		Query query = entityManager.createQuery("update Departement d set d.chefLieu =:cheflieu ,"
				+ "d.nomDep=:nomDep, d.region=:region "
				+ "where d.dep =:dep");
	
	query.setParameter("cheflieu", newDepartement.getChefLieu());
	query.setParameter("nomDep", newDepartement.getNomDep());

	query.setParameter("region", newDepartement.getRegion());
	query.setParameter("dep", newDepartement.getDep());

	
	int nbrUpdate = query.executeUpdate();
	System.out.println(nbrUpdate);

}
}
