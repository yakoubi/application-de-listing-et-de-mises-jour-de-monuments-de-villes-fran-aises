package com.um2.ips.repositories;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.um2.ips.entities.Departement;
import com.um2.ips.entities.Lieu;


@Repository
public class LieuRepository {

	@PersistenceContext
	private EntityManager entityManager;
	
	
	/*
	 * ajouter un departement ds la base 
	 */
	public void insertLieu(Lieu lieu){
		entityManager.persist(lieu);
	}
	
	
	public List<Lieu> listerLieux(){
		List<Lieu> listeLieux = new ArrayList<Lieu>();
		try {
			listeLieux=	entityManager.createQuery("select l from Lieu l").getResultList();

		} catch (Exception e) {
			System.out.println("probleme de recuperation de la liste des departements de la base"+e);
		}
		 return listeLieux;
		
		
	}
	
	/*
	 * suprimer un lieu
	 */
	public void delete(String lieu){
		Lieu lieuSupp= findLieu(lieu);
		entityManager.remove(lieuSupp);
	}
	
	public Lieu  findLieu(String lieuId){
		Lieu lieu= entityManager.find(Lieu.class, lieuId);
return lieu;
	}
	
	public void edit( Lieu newLieu) {
		Query query = entityManager.createQuery("update Lieu l set "
				+ "l.dep.dep =:dep, "
				+ " l.nomCom=:nomCom, "
				+ "l.latitude=:latitude,"
				+ " l.longitude=:longitude "
				+ " where l.codeInsee =:codeInsee");
	System.out.println(newLieu.getDep().getDep());
	query.setParameter("dep", newLieu.getDep().getDep());
	query.setParameter("latitude", newLieu.getLatitude());
	query.setParameter("nomCom", newLieu.getNomCom());

	query.setParameter("longitude", newLieu.getLongitude());
	query.setParameter("codeInsee", newLieu.getCodeInsee());

//	query.setParameter("monument", newLieu.getMonuments());

	
	int nbrUpdate = query.executeUpdate();
	System.out.println(nbrUpdate);

}
}
