package com.um2.ips.repositories;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.um2.ips.entities.Lieu;
import com.um2.ips.entities.Monument;

@Repository
public class MonumentRepository {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	
	@SuppressWarnings("unchecked") // supprimer warning
	public List<Monument> finAll(){
		List<Monument> liste = new ArrayList<Monument>();
		try {
			//les requetes sont ecrites en JPQL
			liste = entityManager.createQuery("from Monument").getResultList();
		} catch (Exception e) {
System.out.println("problème de récupération de la liste de monument de la base"+e);
		}
		 return liste;
	}
	

	/*
	 * ajouter un MONUMENT ds la base 
	 */
	public void insertMonument(Monument monument){
		entityManager.persist(monument);
	}
	
	
	public List<Monument> listerMonuments(){
		List<Monument> listeMonuments = new ArrayList<Monument>();
		try {
			listeMonuments=	entityManager.createQuery("select l from Monument l").getResultList();

		} catch (Exception e) {
			System.out.println("probleme de recuperation de la liste des monuments de la base"+e);
		}
		 return listeMonuments;
		
		
	}
	
	/*
	 * suprimer un monument
	 */
	public void delete(String monument){
		Monument monumentSupp= findMonument(monument);
		entityManager.remove(monumentSupp);
	}
	
	public Monument  findMonument(String monumentId){
		Monument monument= entityManager.find(Monument.class, monumentId);
return monument;
	}
	
	
	public void edit( Monument newMonument) {
		Query query = entityManager.createQuery("update Monument l set "
				+ "l.localite.codeInsee =:codeLieu, "
				+ " l.nomM=:nomM, "
				+ "l.latitude=:latitude,"
				+ " l.longitude=:longitude, "
				+ " l.proprietaire=:proprietaire, "
				+ " l.typeMonument=:typeMonument "
				+ " where l.codeM =:codeM");
	System.out.println(newMonument.getLocalite().getCodeInsee());
	query.setParameter("codeLieu", newMonument.getLocalite().getCodeInsee());
	query.setParameter("latitude", newMonument.getLatitude());
	query.setParameter("nomM", newMonument.getNomM());

	query.setParameter("longitude", newMonument.getLongitude());
	query.setParameter("proprietaire", newMonument.getProprietaire());
	query.setParameter("typeMonument", newMonument.getTypeMonument());

	query.setParameter("codeM", newMonument.getCodeM());


	
	int nbrUpdate = query.executeUpdate();
	System.out.println(nbrUpdate);

}
	

}
