package com.um2.ips.repositories;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.um2.ips.entities.User;


@Repository
public class UserRepository {

	@PersistenceContext
	private EntityManager entityManager;
	
	
	/*
	 * ajouter un User ds la base 
	 */
	public void insertUser(User user){
		if(findUserByEmailPassword(user.getEmail(),user.getPassword())==null) {
//			entityManager.persist(user);
			entityManager.merge(user);
		}else {
			System.out.println("un utilisateur avec le meme email et password existe d�j�");
		}
		
	}
	
	
	public List<User> listerUsers(){
		List<User> listeUser = new ArrayList<User>();
		try {
			listeUser=	entityManager.createQuery("select d from User d").getResultList();

		} catch (Exception e) {
			System.out.println("problème de r�cup�ration de la liste des Users de la base"+e);
		}
		 return listeUser;
		
		
	}
	
	/*
	 * ajouter un User ds la base 
	 */
	public void delete(int User){
		User UserSupp= findUser(User);
		entityManager.remove(UserSupp);
	}
	
	public User  findUser(int userId){
		User user= entityManager.find(User.class, userId);
return user;
	}
	
	
	public void edit( User newUser) {
		Query query = entityManager.createQuery("update User d set "
				+ " d.nomUser =:nomUser ,"
				+ " d.prenomUser=:prenomUser,"
				+ " d.email=:email, "
				+ "  d.password=:password, "
				+ "  d.role=:role "

				+ " where d.idUser =:idUser");
	query.setParameter("nomUser", newUser.getNomUser());
	query.setParameter("prenomUser", newUser.getPrenomUser());
	query.setParameter("email", newUser.getEmail());
	query.setParameter("password", newUser.getPassword());
	query.setParameter("idUser", newUser.getIdUser());
	query.setParameter("role", newUser.getRole());


	
	int nbrUpdate = query.executeUpdate();
	System.out.println(nbrUpdate);

}
	
	/**
	 * trouver un user par son email et mot de pass
	 * @param user
	 */
	 public User findUserByEmailPassword(String email, String password) {
		 User user1 =null;
		 Query query= entityManager.createQuery("select u from User u where u.email=:email "
		 		+ "and u.password=:password");
		 
		 query.setParameter("email", email);
			query.setParameter("password", password);

		 List<User> users =query.getResultList();
		 if(!users.isEmpty()) {
			 user1 =users.get(0);
		 }
		  return user1;
		 
	 }
}
