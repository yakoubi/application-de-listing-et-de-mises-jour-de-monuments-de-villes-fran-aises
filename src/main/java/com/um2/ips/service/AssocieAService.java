package com.um2.ips.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.um2.ips.entities.AssocieA;
import com.um2.ips.entities.AssocieAId;
import com.um2.ips.entities.Celebrite;
import com.um2.ips.entities.Lieu;
import com.um2.ips.repositories.AssocieARepository;
import com.um2.ips.repositories.CelebriteRepository;
import com.um2.ips.repositories.LieuRepository;

@Service
@Transactional
public class AssocieAService {
	@Autowired 
	AssocieARepository associeARepository;
	@Autowired
	CelebriteRepository celebriteRepository ;
	
	public void addAssocieA(AssocieA associeA){
		associeARepository.insertAssocieA(associeA);
	}

	
	public List<AssocieA> listerAssocieA(){
		return associeARepository.listerAssocieA();
	}

	
public void delete(AssocieAId associeA){
	this.associeARepository.delete(associeA);
}

public AssocieA find(AssocieAId associeA) {
	 return this.associeARepository.findAssocieA(associeA);
}



/**
 * 
 * @param codeM
 * @return
 */
public List<Celebrite> listeCelebriteAssocieMonument(String codeM){
List<Celebrite>	listeCelebrite = new ArrayList<Celebrite>();
List<AssocieA> listeAssoByCodeM= associeARepository.findListNumSelecriteByCdeM(codeM);
for(AssocieA ele:listeAssoByCodeM) {
	Celebrite celebrite=celebriteRepository.findCelebrite(ele.getAssocieAId().getNumCelebrite());
	listeCelebrite.add(celebrite);
}
	return listeCelebrite;
}


}
