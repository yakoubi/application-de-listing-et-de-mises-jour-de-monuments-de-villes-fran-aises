package com.um2.ips.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.um2.ips.entities.Celebrite;
import com.um2.ips.repositories.CelebriteRepository;

@Service
@Transactional
public class CelebriteService {
	@Autowired 
	CelebriteRepository CelebriteRepository;
	
	
	public void addCelebrite(Celebrite celebrite){
		CelebriteRepository.insertcelebrite(celebrite);
	}

	
	public List<Celebrite> listerCelebrites(){
		return CelebriteRepository.listerCelebrites();
	}

public void delete(int celebrite){
	this.CelebriteRepository.delete(celebrite);
}

public Celebrite find(int celebrite) {
	 return this.CelebriteRepository.findCelebrite(celebrite);
}

public void edit( Celebrite newCelebrite){
	System.out.println("update Celebrite :"+ newCelebrite.getNumCelebrite());
	this.CelebriteRepository.edit(newCelebrite);
}

}
