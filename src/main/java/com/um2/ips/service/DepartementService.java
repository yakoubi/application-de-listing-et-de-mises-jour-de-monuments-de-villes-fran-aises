package com.um2.ips.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.um2.ips.entities.Departement;
import com.um2.ips.repositories.DepartementRepository;

@Service
@Transactional
public class DepartementService {
	@Autowired 
	DepartementRepository departementRepository;
	
	
	public void addDepartement(Departement dep){
		departementRepository.insertDep(dep);
	}

	
	public List<Departement> listerDepatements(){
		return departementRepository.listerDepartements();
	}

public void delete(String dep){
	this.departementRepository.delete(dep);
}

public Departement find(String dep) {
	 return this.departementRepository.findDep(dep);
}

public void edit( Departement newDepartement){
	System.out.println("update departement :"+ newDepartement.getDep());
	this.departementRepository.edit(newDepartement);
}

}
