package com.um2.ips.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.um2.ips.entities.Lieu;
import com.um2.ips.repositories.LieuRepository;

@Service
@Transactional
public class LieuService {
	@Autowired 
	LieuRepository lieuRepository;
	
	
	public void addlieu(Lieu lieu){
		lieuRepository.insertLieu(lieu);
	}

	
	public List<Lieu> listerLieux(){
		return lieuRepository.listerLieux();
	}

public void delete(String lieuId){
	this.lieuRepository.delete(lieuId);
}

public Lieu find(String dep) {
	 return this.lieuRepository.findLieu(dep);
}

public void edit( Lieu newLieu){
	System.out.println("update Lieu :"+ newLieu.getDep());
	this.lieuRepository.edit(newLieu);
}

}
