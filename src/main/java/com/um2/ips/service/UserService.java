package com.um2.ips.service;

import java.util.List;

import javax.transaction.Transactional;

import org.apache.struts2.components.Password;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.um2.ips.entities.User;
import com.um2.ips.repositories.UserRepository;

@Service
@Transactional
public class UserService {
	@Autowired 
	UserRepository userRepository;
	
	
	public void addUser(User user){
		userRepository.insertUser(user);
	}

	
	public List<User> listerUsers(){
		return userRepository.listerUsers();
	}

public void delete(int user){
	this.userRepository.delete(user);
}

public User find(int user) {
	 return this.userRepository.findUser(user);
}

public void edit( User newUser){
	this.userRepository.edit(newUser);
}


public User getUser(String email, String password) {
	 return userRepository.findUserByEmailPassword(email,password);
}
}
