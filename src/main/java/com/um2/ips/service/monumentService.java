package com.um2.ips.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.um2.ips.entities.Lieu;
import com.um2.ips.entities.Monument;
import com.um2.ips.repositories.MonumentRepository;

@Transactional	
@Service
public class monumentService {
	
	@Autowired
	MonumentRepository monumentRepository;
	
	/**
	 * 
	 * @return
	 */
  public List<Monument> findAll(){
	return  monumentRepository.finAll();
  }
  
  
  
	public void addMonument(Monument monument){
		monumentRepository.insertMonument(monument);
	}

	
	public List<Monument> listerMonuments(){
		return monumentRepository.listerMonuments();
	}

public void delete(String monumentId){
	this.monumentRepository.delete(monumentId);
}

public Monument find(String monumentId) {
	 return this.monumentRepository.findMonument(monumentId);
}

public void edit( Monument newMonument){
	System.out.println("update monument :"+ newMonument.getCodeM());
	this.monumentRepository.edit(newMonument);
}

public double calculerDistance(Monument monument1,Monument monument2) {
	double distance =calculer(monument1.getLatitude(), monument1.getLongitude(), monument2.getLatitude(), monument2.getLongitude(), "K");
	
	return distance;
}

/**
 * calculer la distance entre deux monuments
 * @param lat1
 * @param lon1
 * @param lat2
 * @param lon2
 * @param unit
 * @return
 */
private  double calculer(double lat1, double lon1, double lat2, double lon2, String unit) {
	if ((lat1 == lat2) && (lon1 == lon2)) {
		return 0;
	}
	else {
		double theta = lon1 - lon2;
		double dist = Math.sin(Math.toRadians(lat1)) * Math.sin(Math.toRadians(lat2)) + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.cos(Math.toRadians(theta));
		dist = Math.acos(dist);
		dist = Math.toDegrees(dist);
		dist = dist * 60 * 1.1515;
		if (unit.equals("K")) {
			dist = dist * 1.609344;
		} else if (unit.equals("N")) {
			dist = dist * 0.8684;
		}
		return (dist);
	}
}
}
