<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!-- <link rel="stylesheet" type="text/css" href="../css/style.css"> -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<sb:head />

<title>Ajouter une nouvelle commune</title>
</head>
<body>
<!-- le menu -->
	<s:include value="../../menu.jsp"></s:include>
<!-- zone d'affichage des message  -->
	<s:actionerror theme="bootstrap" />
	<s:actionmessage theme="bootstrap" />
	<s:fielderror theme="bootstrap" />
	<!-- si l'utlisateur n'est pas autentifier il sera rediriger vers la page login -->
<s:if test="#session.logined != 'true'">
<jsp:forward page="login.jsp" />
</s:if>
	<div class="col-md-10">
		<!-- attribuer les droit selon les roles -->
		<s:if test="#session.logined = 'true' && #session.user.role =='admin'">
	<!-- formulaire -->
	
		<h3 class="col-md-offset-5">Ajouter une nouvelle célébrité au monument: <s:property value="monumentEcours.nomM"/> - <s:property value="monumentEcours.typeMonument"/></h3>
		<s:form action="addAssociea"   enctype="multipart/form-data"
			theme="bootstrap" cssClass="form-horizontal">
				
			<s:textfield name="monumentId" 
			label="monumentId"></s:textfield>
			<s:select label="célebrité" headerKey="a"
 				headerValue="ajouter une célébrité"
 				 list="listeCelebrites2" 
 				name="celebriteSel" required="true"/> 
			
<%-- 			<s:textfield name="lieu.nomCom" label="nom du lieu"></s:textfield> --%>
			
			
			
			<s:hidden name="modeEdtit" label="modeEdtit"></s:hidden>
			<div class="form-group">
				<div class="col-sm-offset-10 ">
					<s:submit cssClass="btn btn-primary" />
				</div>
			</div>
		</s:form>
</s:if>

		<!--  liste des departements  -->

		<h3 class="col-md-offset-5">Liste des communes</h3>
		<table class="table table-hover">
			<tr>
<!-- 				<th>num Celebrite</th> -->
<!-- 				<th>code monument</th> -->
				<th>monument id </th>
				<th>numéro de célébrité</th>
				<th>nom</th>
				<th>prénom</th>
				<th>nationalité</th>
				<th>epoque</th>
					<s:if test="#session.logined = 'true' && #session.user.role =='admin'">
				
				<th>action</th>
				</s:if>
			</tr>
				<s:iterator value="listeCelebritesParCodeM">
				<tr>
					<td><s:property value="monumentId" /></td>
					<td><s:property value="numCelebrite" /></td>
					<td><s:property value="nom" /></td>
					<td><s:property value="prenom" /></td>
					<td><s:property value="nationalite" /></td>
					<td><s:property value="epoque" /></td>
					
					<s:url action="delete" var="lien1">
 						<s:param name="celebriteSel">
							<s:property value="numCelebrite" />
						</s:param>
						<s:param name="monumentId">
							<s:property value="monumentId" />
						</s:param>
					</s:url>
<%-- 					<s:url action="edit" var="lien2"> --%>
<%-- 						<s:param name="lieuId"> --%>
<%-- 							<s:property value="codeInsee" /> --%>
<%-- 						</s:param> --%>
<%-- 					</s:url> --%>
	<s:if test="#session.logined = 'true' && #session.user.role =='admin'">

					<td><s:a href="%{lien1}"> supprimer</s:a></td>
<%-- 					<td><s:a href="%{lien2}"> editer</s:a></td> --%>
				</s:if>
				</tr>
			</s:iterator>

		</table>
	</div>
</body>
</html>