<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!-- <link rel="stylesheet" type="text/css" href="../css/style.css"> -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<sb:head />

<title>Ajouter un département</title>
</head>


<body>

<!-- le menu -->
	<s:include value="../../menu.jsp"></s:include>

<!-- zone d'affichage des message  -->
	<s:actionerror theme="bootstrap" />
	<s:actionmessage theme="bootstrap" />
	<s:fielderror theme="bootstrap" />

<!-- si l'utlisateur n'est pas autentifier il sera rediriger vers la page login -->
<s:if test="#session.logined != 'true'">
<jsp:forward page="login.jsp" />
</s:if>

	<div class="col-md-10">
	<!-- attribuer les droit selon les roles -->
	<s:if test="#session.logined = 'true' && #session.user.role =='admin'">
<!-- formulaire -->
		<h3 class="col-md-offset-5">Ajouter un nouveau département</h3>

		<s:form action="addDepartement" enctype="multipart/form-data"
			theme="bootstrap" cssClass="form-horizontal">
			<s:textfield name="dep.dep" label="numero de departement"></s:textfield>
			<s:textfield name="dep.chefLieu" label="chef lieu"></s:textfield>
			<s:textfield name="dep.nomDep" label="nom de département"></s:textfield>
			<s:textfield name="dep.region" label="région"></s:textfield>
			<s:hidden name="modeEdtit" label="modeEdtit"></s:hidden>
			
			
			<div class="form-group">
				<div class="col-sm-offset-10 ">
					<s:submit cssClass="btn btn-primary" />
				</div>
			</div>
		</s:form>
</s:if>
<!-- talbeau -->
		<!--  liste des departements  -->

		<h3 class="col-md-offset-5">la Liste des départements</h3>
		<table class="table table-hover">
			<tr>
				<th>numéro de département</th>
				<th>chef lieu</th>
				<th>nom de département</th>
				<th>région</th>
				<s:if test="#session.logined = 'true' && #session.user.role =='admin'">
				<th>action</th>
				</s:if>
			</tr>
			<s:iterator value="listeDepartements">
				<tr>
					<td><s:property value="dep" /></td>
					<td><s:property value="chefLieu" /></td>
					<td><s:property value="nomDep" /></td>
					<td><s:property value="region" /></td>
					<s:url action="delete" var="lien1">
						<s:param name="depId">
							<s:property value="dep" />
						</s:param>
					</s:url>
					<s:url action="edit" var="lien2">
						<s:param name="depId">
							<s:property value="dep" />
						</s:param>
					</s:url>
					<s:if test="#session.logined = 'true' && #session.user.role =='admin'">
					<td><s:a href="%{lien1}"> supprimer</s:a></td>
					<td><s:a href="%{lien2}"> editer</s:a></td>
					</s:if>
				</tr>
			</s:iterator>
			<%-- 		<c:forEach var="dep" items="${listeDepartements }"> --%>
			<%-- 			<tr> --%>
			<%-- 			<td> <s:property value="/></td> --%>
			<%-- 				<td> ${dep.dep }</td> --%>
			<%-- 				<td>${dep.chefLieu}</td> --%>
			<%-- 				<td>${dep.nomDep}</td> --%>
			<%-- 				<td>${dep.region}</td> --%>
			<%-- 				<s:url  action="delete" var="lien1"> --%>
			<%-- 						<s:param name="depId">${dep.dep} </s:param> --%>
			<%-- 					</s:url> --%>
			<%-- 				<s:url action="edit" var="lien2"> --%>
			<%-- 						<s:param name="depId">${dep.dep} </s:param> --%>
			<%-- 					</s:url> --%>

			<%-- 				<td><s:a href="%{lien1}"> supprimer</s:a></td> --%>
			<%-- 				<td><s:a href="%{lien2}"> editer</s:a></td> --%>

			<%-- 			</tr> --%>
			<%-- 		</c:forEach> --%>
		</table>


	</div>
</body>
</html>