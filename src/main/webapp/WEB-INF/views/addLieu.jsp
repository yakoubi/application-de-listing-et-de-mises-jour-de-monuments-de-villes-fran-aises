<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!-- <link rel="stylesheet" type="text/css" href="../css/style.css"> -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<sb:head />

<title>Ajouter une nouvelle commune</title>
</head>
<body>

<!-- le menu -->
	<s:include value="../../menu.jsp"></s:include>

<!-- zone d'affichage des message  -->
	<s:actionerror theme="bootstrap" />
	<s:actionmessage theme="bootstrap" />
	<s:fielderror theme="bootstrap" />

<!-- si l'utlisateur n'est pas autentifier il sera rediriger vers la page login -->
<s:if test="#session.logined != 'true'">
<jsp:forward page="login.jsp" />
</s:if>

	<div class="col-md-10">
	<!-- attribuer les droit selon les roles -->
	<s:if test="#session.logined = 'true' && #session.user.role =='admin'">
<!-- formulaire -->	
		<h3 class="col-md-offset-5">Ajouter une nouvelle commune</h3>
		<s:form action="addLieu"  enctype="multipart/form-data"
			theme="bootstrap" cssClass="form-horizontal">
			<s:textfield name="lieu.codeInsee" label="codeIsee"></s:textfield>
			<%-- 		<s:textfield name="lieu.dep.dep" label="departement" ></s:textfield> --%>
			
<%-- 			<s:select label="departement1" headerKey="a" --%>
<%-- 				headerValue="selectioner un dep" list="listeDepartementsDep" --%>
<%-- 				name="lieu.dep.dep" /> --%>
				
				
				
				<s:select label="departement2" headerKey="a"
				headerValue="selectioner un departement" list="listeDepartementsDep2"
				name="lieu.dep.dep" />
				
				
<%-- 				<s:select label="departement3"  list="listeDepartements" --%>
<%-- 				listValue="lieu.dep.dep" --%>
<%-- 				/>				 --%>
				
<%-- 				<s:select label="departement4"  --%>
<%-- 				headerKey="a" --%>
<%-- 				headerValue="ajouter une c�l�brit�"  --%>
<%-- 				list="listeDepartementsDep2" --%>
<%-- 				listKey="lieu.dep" --%>
<%-- 				listValue="lieu.nomDep" --%>
<%-- 				value="nomDep" --%>
<%-- 				name="lieu.dep.dep"  --%>
<%-- 				required="true"/> --%>
				
<%-- 				<s:select label="Select a month5"  --%>
<%-- 		headerKey="-1" headerValue="Select Month" --%>
<%-- 		list="listeDepartementsDep2"  --%>
<%-- 		name="yourMonth"  --%>
<%-- 		value="lieu.dep.dep" /> --%>
				
				
				
				
			<s:textfield name="lieu.nomCom" label="nom du lieu"></s:textfield>
			<s:textfield name="lieu.latitude" label="latitude"></s:textfield>
			<s:textfield name="lieu.longitude" label="longitude"></s:textfield>
			<%-- 				<s:textfield name="lieu.monument" label="r�gion" ></s:textfield> --%>

			<s:hidden name="modeEdtit" label="modeEdtit"></s:hidden>
			<div class="form-group">
				<div class="col-sm-offset-10 ">
					<s:submit cssClass="btn btn-primary" />
				</div>
			</div>
		</s:form>
</s:if>

		<!--  liste des departements  -->

		<h3 class="col-md-offset-5">Liste des communes</h3>
		<table class="table table-hover">
			<tr>
				<th>codeInsee</th>
				<th>nom commune</th>
				<th>latitude</th>
				<th>longitude</th>
				<th>departement</th>
				<th>num departement</th>
					<s:if test="#session.logined = 'true' && #session.user.role =='admin'">
				
				<th>action</th>
				</s:if>
			</tr>
			<s:iterator value="listeLieux">
				<tr>
					<td><s:property value="codeInsee" /></td>
					<td><s:property value="nomCom" /></td>
					<td><s:property value="latitude" /></td>
					<td><s:property value="longitude" /></td>
					<td><s:property value="dep.dep" /></td>
					<td><s:property value="dep.nomDep" /></td>
					<s:url action="delete" var="lien1">
						<s:param name="lieuId">
							<s:property value="codeInsee" />
						</s:param>
					</s:url>
					<s:url action="edit" var="lien2">
						<s:param name="lieuId">
							<s:property value="codeInsee" />
						</s:param>
					</s:url>
	<s:if test="#session.logined = 'true' && #session.user.role =='admin'">

					<td><s:a href="%{lien1}"> supprimer</s:a></td>
					<td><s:a href="%{lien2}"> editer</s:a></td>
			</s:if>
				</tr>
			</s:iterator>

		</table>
	</div>
</body>
</html>