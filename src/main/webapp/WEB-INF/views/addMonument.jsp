<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!-- <link rel="stylesheet" type="text/css" href="../../css/style.css"> -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<sb:head />

<title>Ajouter un monument</title>
</head>
<body>

	<!-- le menu -->
	<s:include value="../../menu.jsp"></s:include>

	<!-- zone d'affichage des message  -->
	<s:actionerror theme="bootstrap" />
	<s:actionmessage theme="bootstrap" />
	<s:fielderror theme="bootstrap" />

	<!-- si l'utlisateur n'est pas autentifier il sera rediriger vers la page login -->
	<s:if test="#session.logined != 'true'">
		<jsp:forward page="login.jsp" />
	</s:if>

	<div class="col-md-10">


		<div class="container">
			<div class="row">
				<div class="col-md-5">
					<!-- attribuer les droit selon les roles -->
					<s:if
						test="#session.logined = 'true' && #session.user.role =='admin'">
						<!-- formulaire -->

						<h3 class="col-md-offset-5">Ajouter un nouveau monument</h3>
						<s:form action="addMonument" enctype="multipart/form-data"
							theme="bootstrap" cssClass="form-horizontal">
							<s:textfield name="monument.codeM" label="codeM"></s:textfield>
							<%-- 		<s:textfield name="monument.localite" label="localite" ></s:textfield> --%>
							<s:select label="localite"
							 headerKey="a"
								headerValue="selectioner une localite"
								list="listeLieuxCodeInsee" 
								name="monument.localite.codeInsee" 
								required="true"/>
							<s:textfield name="monument.nomM" label="nom du monument"></s:textfield>

							<s:textfield name="monument.proprietaire" label="proprietaire"></s:textfield>

							<s:textfield name="monument.typeMonument"
								label="type de monument"></s:textfield>
							<s:textfield name="monument.latitude" label="latitude"></s:textfield>
							<s:textfield name="monument.longitude" label="longitude"></s:textfield>
							<%-- 				<s:textfield name="monument.monument" label="r�gion" ></s:textfield> --%>

									<s:hidden name="modeEdtit" label="modeEdtit"></s:hidden>
							<div class="form-group">
								<div class="col-sm-offset-10 ">
									<s:submit cssClass="btn btn-primary" />
								</div>
							</div>

						</s:form>
					</s:if>


				</div>
				<div class="col-md-5">
					<h3 class="col-md-offset-5">calculer la distance entre
						monument</h3>
					<s:form action="distance" enctype="multipart/form-data"
						theme="bootstrap" cssClass="form-horizontal">
						<s:textfield name="monument1.codeM" label="codeM 1"></s:textfield>
						<s:textfield name="monument2.codeM" label="codeM 2"></s:textfield>
						<s:textfield name="distance" label="distance(km)"></s:textfield>
						<div class="form-group">
							<div class="col-sm-offset-10 ">
								<s:submit cssClass="btn btn-primary" />
							</div>
						</div>
					</s:form>

				</div>
			</div>

			<div class=row>

				<!--  liste des departements  -->

				<h3 class="col-md-offset-5">Liste des monuments</h3>
				<table class="table table-hover">
					<tr>
						<th>codem</th>
						<th>nom monument</th>
						<th>latitude</th>
						<th>longitude</th>
						<th>proprietaire</th>
						<th>type de monument</th>
						<th>commune</th>
						<th>num commune</th>

						<th>action</th>
					</tr>
					<s:iterator value="monuments">
						<tr>
							<td><s:property value="codeM" /></td>
							<td><s:property value="nomM" /></td>
							<td><s:property value="latitude" /></td>
							<td><s:property value="longitude" /></td>
							<td><s:property value="proprietaire" /></td>
							<td><s:property value="typeMonument" /></td>
							<td><s:property value="localite.codeInsee" /></td>
							<td><s:property value="localite.nomCom" /></td>
							<s:url action="delete" var="lien1">
								<s:param name="monumentId">
									<s:property value="codeM" />
								</s:param>
							</s:url>
							<s:url action="edit" var="lien2">
								<s:param name="monumentId">
									<s:property value="codeM" />
								</s:param>
							</s:url>

							<s:url namespace="/associeA" action="associeA" var="lien3">
								<s:param name="monumentId">
									<s:property value="codeM" />
								</s:param>
							</s:url>
							<s:if
								test="#session.logined = 'true' && #session.user.role =='admin'">

								<td><s:a href="%{lien1}"> supprimer</s:a></td>
								<td><s:a href="%{lien2}"> editer</s:a></td>
							</s:if>
							<td><s:a href="%{lien3}"> d�tail</s:a></td>

						</tr>
					</s:iterator>

				</table>
			</div>
		</div>
	</div>
</body>
</html>