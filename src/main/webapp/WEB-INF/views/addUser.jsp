<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!-- <link rel="stylesheet" type="text/css" href="../css/style.css"> -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<sb:head />

<title>Ajouter un utilisateur</title>
</head>


<body>

<!-- le menu -->
	<s:include value="../../menu.jsp"></s:include>

<!-- zone d'affichage des message  -->
	<s:actionerror theme="bootstrap" />
	<s:actionmessage theme="bootstrap" />
	<s:fielderror theme="bootstrap" />

<!-- si l'utlisateur n'est pas autentifier il sera rediriger vers la page login -->
<%-- <s:if test="#session.logined != 'true'"> --%>
<%-- <jsp:forward page="login.jsp" /> --%>
<%-- </s:if> --%>

	<div class="col-md-10">
	<!-- attribuer les droit selon les roles -->
<!-- formulaire -->
		<h3 class="col-md-offset-5">Ajouter un nouveau utilisateur</h3>

		<s:form action="addUser" enctype="multipart/form-data"
			theme="bootstrap" cssClass="form-horizontal">
<%-- 			<s:textfield name="user.idUser" label="identifiant "></s:textfield> --%>
			<s:textfield name="user.nomUser" label="nom "></s:textfield>
			<s:textfield name="user.prenomUser" label="pr�nom "></s:textfield>
			<s:textfield name="user.email" label="email"></s:textfield>
			<s:textfield name="user.password" label="mot de passe"></s:textfield>
			<s:if test="#session.logined = 'true' && #session.user.role =='admin'">
			<s:textfield name="user.role" label="r�le"></s:textfield>
			</s:if>
			<s:hidden name="modeEdtit" label="modeEdtit"></s:hidden>
			
			<div class="form-group">
				<div class="col-sm-offset-10 ">
					<s:submit cssClass="btn btn-primary" />
				</div>
			</div>
		</s:form>

		<!--  liste des utilisateur -->
	    <s:if test="#session.logined = 'true' && #session.user.role =='admin'">
		<h3 class="col-md-offset-5">la Liste des utilisateurs</h3>
		<table class="table table-hover">
			<tr>
				<th>userId</th>
				<th>nom de l'utilisateur</th>
				<th>pr�nom de l'utilisateur</th>
				<th>email</th>
				<th>password</th>
			</tr>
			<s:iterator value="listeUsers">
				<tr>
					<td><s:property value="idUser" /></td>
					<td><s:property value="nomUser" /></td>
					<td><s:property value="prenomUser" /></td>
					<td><s:property value="email" /></td>
				    <td><s:property value="password" /></td>
				    <td><s:property value="role" /></td>
					<s:url action="delete" var="lien1">
						<s:param name="idUser">
							<s:property value="idUser" />
						</s:param>
					</s:url>
					<s:url action="edit" var="lien2">
						<s:param name="idUser">
							<s:property value="idUser" />
						</s:param>
					</s:url>

					<td><s:a href="%{lien1}"> supprimer</s:a></td>
					<td><s:a href="%{lien2}"> editer</s:a></td>
				</tr>
			</s:iterator>
			
		</table>

</s:if>

	</div>
	
</body>
</html>