<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Spring and Struts 2 Integration</title>
</head>
<body>

	<s:property value="msg" />
	
<h3>liste des monuments</h3>
	<table border="1">
		<tr>
			<th>codeM</th>
			<th>nomM</th>
			<th>propriétaire</th>
			<th>typeMonument</th>
			<th>latitude</th>
			<th>longitude</th>
		</tr>
		<c:forEach var="monument" items="${monuments }">
			<tr>
				<td>${monument.codeM }</td>
				<td>${monument.nomM }</td>
				<td>${monument.proprietaire }</td>
				<td>${monument.typeMonument }</td>
				<td>${monument.latitude }</td>
				<td>${monument.longitude }</td>
			</tr>
		</c:forEach>
	</table>

</body>
</html>