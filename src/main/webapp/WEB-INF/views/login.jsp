<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!-- <link rel="stylesheet" type="text/css" href="../css/style.css"> -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<sb:head />

<title>Ajouter un département</title>
</head>


<body>
	<s:actionerror theme="bootstrap" />
	<s:actionmessage theme="bootstrap" />
	<s:fielderror theme="bootstrap" />

	<s:include value="../../menu.jsp"></s:include>

	<div class="col-md-8">


		<s:form action="login" enctype="multipart/form-data" theme="bootstrap"
			cssClass="form-horizontal">
			<s:textfield name="email" label="email"></s:textfield>
			<s:textfield name="password" label="mot de passe"></s:textfield>
			<div class="form-group">
				<div class="col-sm-offset-9 ">
					<s:submit cssClass="btn btn-primary" />
				</div>
				<div class="col-md-offset-10">
					<s:url namespace="/user" action="users" var="lien1"></s:url>
					<s:a href="%{lien1}"> creer un compte </s:a>

				</div>
			</div>
		</s:form>



	</div>
</body>
</html>