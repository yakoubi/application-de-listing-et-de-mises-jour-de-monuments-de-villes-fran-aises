<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Spring and Struts 2 Integration</title>
<link rel="stylesheet" type="text/css" href="css/style.css">
<style>
img {
  width: 100%;
  height: auto;
}
</style>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<sb:head />
</head>
<body>

	<s:include value="menu.jsp"></s:include>
	
<%-- <s:if test="#session.logined != 'true'"> --%>
<%-- <jsp:forward page="/WEB-INF/views/login.jsp" /> --%>
<%-- </s:if> --%>
	<div class="col-md-12 ">
	
	<img src="la-tour-eiffel-en-robe.jpg" width="460" height="345">
	
	</div>
</body>
</html>