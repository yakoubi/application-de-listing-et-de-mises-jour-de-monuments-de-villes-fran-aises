<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Spring and Struts 2 Integration</title>
<link rel="stylesheet" type="text/css" href="css/style.css">
<style>

</style>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<sb:head />
</head>
<body>

	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">GESTION MONUMENT</a>
			</div>
			<ul class="nav navbar-nav navbar-left">

				
				<li><s:url  namespace ="/" action="home" var="lien1"></s:url>
					<s:a href="%{lien1}"> home</s:a></li>
				

				<li><s:url namespace="/departement" action="departements"
						var="lien1"></s:url> <s:a href="%{lien1}"> departements </s:a></li>
						
				<li><s:url namespace="/lieu" action="lieux" var="lien1"></s:url>
					<s:a href="%{lien1}"> lieux </s:a></li>
					
				<li><s:url namespace="/monument" action="monuments" var="lien1"></s:url>
					<s:a href="%{lien1}"> monuments </s:a></li>
					
					
				<li><s:url namespace="/celebrite" action="celebrites" var="lien1"></s:url>
					<s:a href="%{lien1}"> celebrites </s:a></li>
					
<%-- 					<li><s:url namespace="/associeA" action="associeA" var="lien1"></s:url> --%>
<%-- 					<s:a href="%{lien1}"> associeA </s:a></li> --%>
					
						<s:if test="#session.logined = 'true' && #session.user.role =='admin'">
					<li><s:url namespace="/user" action="users" var="lien1"></s:url>
					<s:a href="%{lien1}"> utilisateurs </s:a></li>
					</s:if>
					
				<li><s:url  namespace="/" action="login" var="lien1"></s:url>
					<s:a href="%{lien1}"> login </s:a></li>
					
					<li><s:url  namespace="/" action="logout" var="lien1"></s:url>
					<s:a href="%{lien1}"> logout</s:a></li>
					
				<li class="navbar-text">
<%-- 				<s:property value="#session.user.email"/> --%>
					<s:property value="#session.user.nomUser"/>
					<s:property value="#session.user.role"/>
					 </li>

			</ul>
		</div>
	</nav>
	
</body>
</html>